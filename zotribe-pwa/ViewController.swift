//
//  ViewController.swift
//  zotribe-pwa
//
//  Created by Sailesh Sahu on 28/01/20.
//  Copyright © 2020 Sailesh Sahu. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate{
var text = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func buttonClicked(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        if let controller = storyboard?.instantiateViewController(identifier: "WKWebviewViewController") as? WKWebviewViewController {
            controller.url = text
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        text = textField.text ?? ""
        return true
    }
}

