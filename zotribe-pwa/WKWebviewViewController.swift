//
//  WKWebviewViewController.swift
//  zotribe-pwa
//
//  Created by Sailesh Sahu on 28/01/20.
//  Copyright © 2020 Sailesh Sahu. All rights reserved.
//

import UIKit
import WebKit

class WKWebviewViewController: UIViewController {

    @IBOutlet weak var customWebView: WKWebView!
    var url: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        if let actualURL = URL.init(string: url) {
            customWebView.load(URLRequest(url: actualURL))
        }
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
